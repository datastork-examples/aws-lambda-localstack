package io.datastork.localstack.example;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.serverless.proxy.internal.testutils.MockLambdaContext;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LambdaTest {

  private static final String BUCKET_PERSON = "person";

  private static AmazonS3 amazonS3;
  private static Lambda lambda;

  private final ObjectMapper objectMapper = new ObjectMapper();
  private final Context lambdaContext = new MockLambdaContext();

  private Set<String> resourcesToBeCleanedUp;

  @BeforeAll
  static void init() {
    amazonS3 = AmazonS3ClientBuilder.standard()
      .withEndpointConfiguration(
        new AwsClientBuilder.EndpointConfiguration(
          "http://localhost:4572", "eu-west-1"))
      .withPathStyleAccessEnabled(true)
      .build();

    amazonS3.createBucket(BUCKET_PERSON);
  }

  @AfterAll
  static void tear() {
    amazonS3.deleteBucket(BUCKET_PERSON);
  }

  @BeforeEach
  void setUp() {
    lambda = new Lambda(amazonS3);
    resourcesToBeCleanedUp = new HashSet<>();
  }

  @AfterEach
  void tearDown() {
    cleanupS3Resources();
  }

  @Test
  void handleRequest() throws IOException {
    final InputStream dynamoEventStream = getClass()
      .getClassLoader()
      .getResourceAsStream("dynamo-db-event.json");

    final DynamodbEvent testEvent =
      objectMapper.readValue(dynamoEventStream, DynamodbEvent.class);

    final String id = lambda.handleRequest(testEvent, lambdaContext);
    resourcesToBeCleanedUp.add(id);

    final S3Object person = amazonS3.getObject("person", id);
    assertEquals("Peter Parker", transformInput(person.getObjectContent()));
  }

  private void cleanupS3Resources() {
    resourcesToBeCleanedUp.forEach(id -> amazonS3.deleteObjects(
      new DeleteObjectsRequest(BUCKET_PERSON).withKeys(id)));
  }

  private static String transformInput(final InputStream input) throws IOException {
    final BufferedReader reader = new BufferedReader(new InputStreamReader(input));
    final StringBuilder result = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      result.append(line);
    }

    return result.toString();
  }

}

