package io.datastork.localstack.example;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.util.Map;
import java.util.Random;

public class Lambda implements RequestHandler<DynamodbEvent, String> {

  private final Random random = new Random();
  private final AmazonS3 amazonS3;

  public Lambda() {
    amazonS3 = AmazonS3ClientBuilder
      .standard()
      .build();
  }

  Lambda(final AmazonS3 amazonS3) {
    this.amazonS3 = amazonS3;
  }

  @Override
  public String handleRequest(final DynamodbEvent dynamodbEvent, final Context context) {
    final Map<String, AttributeValue> newImage = dynamodbEvent
      .getRecords()
      .get(0)
      .getDynamodb()
      .getNewImage();

    final String id = String.valueOf(random.nextInt());
    final String name = newImage.get("name").getS();

    amazonS3.putObject("person", String.valueOf(id), name);

    return id;
  }

}
